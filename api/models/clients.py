from api import db
from datetime import datetime

# TODO: Integer choices for status

class Clients(db.Model):
  """This class represents the clients table."""

  __tablename__ = 'clients'

  client_id = db.Column(db.Integer, primary_key=True, autoincrement = True)
  client_name = db.Column(db.String(50), nullable = False)
  added_by = db.Column(db.Integer, db.ForeignKey('users.user_id'),  nullable = False)
  status = db.Column(db.Integer, nullable = False)
  date_added = db.Column(db.DateTime, default=db.func.current_timestamp(), nullable = False)
  date_updated = db.Column(db.DateTime, default=db.func.current_timestamp())

  def __init__(self, name, admin_id):
    """initialize with name."""
    self.client_name = name
    self.added_by = admin_id
    self.date_added = datetime.now()
    self.status = 1

  def save(self):
    db.session.add(self)
    db.session.commit()

  @staticmethod
  def get_all():
    return Clients.query.all()

  def delete(self):
    db.session.delete(self)
    db.session.commit()

  def __repr__(self):
    return "".format(self.client_name)
