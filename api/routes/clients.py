from flask import (
  Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
from werkzeug.security import check_password_hash, generate_password_hash

from api import db
from api.models.clients import Clients
from api.models.users import Users

# bp = Blueprint('auth', __name__, url_prefix='/auth')
bp = Blueprint('clients', __name__)

@bp.route('/clients', methods=['GET', 'POST'])
def get_post_clients():
  if request.method == 'GET':
    clients = Clients.get_all()
    for client in clients:
      data = {
        'id': client.client_id,
        'name': client.client_name,
        'status': client.status, 
        'date_added': client.date_added
      }
    return jsonify({'message': 'Clients successfully retrieved', 'data': data, 'status_code': 200}), 200
  if request.method == 'POST':
    content = request.json

    # TODO: Get logged in user's user_id
    user = Users.query.filter_by(user_id = 1).first()
    
    # Save client
    client = Clients(content['name'], 1)
    client.save()
    return jsonify({'message': 'Clients successfully added', 'data': client, 'status_code': 200}), 200
  
  return {'message': 'Bad Request', 'status_code': 400}, 400