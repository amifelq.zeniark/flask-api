from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

# local import
from instance.config import app_config

# initialize sql-alchemy
db = SQLAlchemy()
migrate = Migrate()

def create_app(config_name):
  app = Flask(__name__, instance_relative_config=True)
  app.config.from_object(app_config[config_name])
  # app.config.from_pyfile('config.py')
  app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
  db.init_app(app)
  migrate.init_app(app, db)

  # register blueprint/s
  from api.routes import clients
  app.register_blueprint(clients.bp)

  return app