import os

class Config(object):
  DEBUG = False
  TESTING = False
  CSRF_ENABLED = True
  SQLALCHEMY_TRACK_MODIFICATIONS = False
  SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
  SESSION_COOKIE_SECURE = True
  SESSION_COOKIE_HTTPONLY = True
  SESSION_COOKIE_SAMESITE = 'None'
class ProductionConfig(Config):
  DEBUG = False
  SECRET_KEY = "9asdf8980as8df9809sf6a6ds4f3435fa64ˆGggd76HSD57hsˆSDnb"
  SQLALCHEMY_TRACK_MODIFICATIONS = False
class DevelopmentConfig(Config):
  ENV = "development"
  DEVELOPMENT = True
  SECRET_KEY = "9asdf8980as8df9809sf6a6ds4f3435fa64ˆGggd76HSD57hsˆSDnb"
  OAUTHLIB_INSECURE_TRANSPORT = True
  SQLALCHEMY_TRACK_MODIFICATIONS = True


app_config = {
  'development': DevelopmentConfig,
  'production': ProductionConfig,
}
